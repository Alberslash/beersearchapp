//
//  AdvancedSearchViewController.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 29/9/22.
//

import Foundation
import UIKit
import RxSwift

class AdvancedSearchViewController: UIViewController {

    @IBOutlet var container: UIView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var fistSliderValue: UILabel!
    @IBOutlet weak var titleFirstFilter: UILabel!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!

    private let disposedBag = DisposeBag()
    
    var presenter: AdvancedSearchPresenterProtocol?

    @IBAction func sliderAction(_ sender: UISlider) {
        let currentValue = Int(sender.value)
        self.fistSliderValue.text = "\(currentValue)"
        presenter?.filterByABV(value: currentValue)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpObservers()
        setUpTableView()
        setUpSlider()
        setUpLabels()
    }
}

extension AdvancedSearchViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.numberOfItems() ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: BeerTableViewCell.self)) as? BeerTableViewCell {
            if let beerEntity = presenter?.getBeerAt(atIndexPath: indexPath) {
                cell.configure(beer: beerEntity)
                cell.selectionStyle = .none
                return cell
            }
        }
        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let id = presenter?.getBeerAt(atIndexPath: indexPath)?.id {
            presenter?.navigateToBeerDetail(id: id)
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
}

extension AdvancedSearchViewController: AdvancedSearchViewProtocol {
    func loadData() {
        self.tableview.reloadData()
    }

    func hideLoading() {

    }

    func showLoading() {

    }
}

private extension AdvancedSearchViewController {

    func setUpSlider() {
        self.slider.minimumValue = 0
        self.slider.maximumValue = 17
        self.slider.setValue(0, animated: false)
    }

    func setUpLabels() {
        self.titleFirstFilter.text = Localized.AbvPercentage
        self.fistSliderValue.text = "0"
        self.titleLabel.text = Localized.SearchBeersToEat
        self.titleLabel.numberOfLines = 2
        self.titleLabel.font = UIFont.boldSystemFont(ofSize: 16)
    }

    func setUpObservers() {
        self.searchBar.rx.text
            .subscribe(onNext: { [unowned self] text in
                if let food = text, food.count >= 4 {
                    presenter?.getBeersByFood(food: food)
                    slider.isEnabled = true
                } else {
                    slider.isEnabled = false
                }
                if let texto = text, texto.count > 0 {
                    self.titleLabel.text = Localized.SearchingBeersToEat + " \(texto)"
                } else {
                    self.titleLabel.text = Localized.SearchBeersToEat
                    self.presenter?.setIsFiltering(false)
                    self.presenter?.cleanList()
                    self.slider.setValue(0, animated: true)
                    self.fistSliderValue.text = "0"
                }
                self.tableview.reloadData()
            }).disposed(by: disposedBag)
    }

    func setUpTableView() {
        self.tableview.backgroundColor = .white
        self.tableview.delegate = self
        self.tableview.dataSource = self
        self.tableview.showsVerticalScrollIndicator = false

        let beerCellCellIdentifier = String(describing: BeerTableViewCell.self)
        tableview.register(UINib(nibName: beerCellCellIdentifier, bundle: nil), forCellReuseIdentifier: beerCellCellIdentifier)
    }
}
