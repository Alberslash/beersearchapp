//
//  AdvancedSearchConfigurator.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 29/9/22.
//

import Foundation

class AdvancedSearchConfigurator {

    class func configureAdvancedSearchScene() -> AdvancedSearchViewController {
        let viewController = AdvancedSearchViewController()
        let router = AdvancedSearchRouter()
        let presenter = AdvancedSearchPresenter(
            view: viewController,
            useCase: GetAllBeersByFoodUseCase(service: BeerService()),
            router: router,
            allBersEntity: AllBeersEntity(allBeers: []))

        viewController.presenter = presenter
        router.viewController = viewController

        return viewController
    }
}
