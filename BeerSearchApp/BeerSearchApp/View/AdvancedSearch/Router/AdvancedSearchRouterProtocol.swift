//
//  AdvancedSearchRouterProtocol.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 30/9/22.
//

import Foundation

protocol AdvancedSearchRouterProtocol {

    func pushBeerDetail(id: Int)
}
