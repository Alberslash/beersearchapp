//
//  AdvancedSearchRouter.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 30/9/22.
//

import UIKit

class AdvancedSearchRouter: AdvancedSearchRouterProtocol {

    weak var viewController: UIViewController?
    
    func pushBeerDetail(id: Int) {
        let beerDetailVC = BeerDetailConfigurator.configureBeerDetailScene(id: id)
        viewController?.navigationController?.pushViewController(beerDetailVC, animated: true)
    }
}
