//
//  AdvancedSearchPresenterProtocol.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 29/9/22.
//

import Foundation

protocol AdvancedSearchPresenterProtocol {
    func getBeersByFood(food: String)
    func getBeerAt(atIndexPath indexPath: IndexPath) -> BeerEntity?
    func numberOfItems() -> Int?
    func cleanList()
    func filterByABV(value: Int)
    func setIsFiltering(_ isFiltering: Bool)
    func navigateToBeerDetail(id: Int)
}
