//
//  AdvancedSearchPresenter.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 29/9/22.
//

import Foundation
import RxSwift

class AdvancedSearchPresenter  {

    private var useCase: GetAllBeersByFoodUseCaseProtocol?
    private var router: AdvancedSearchRouterProtocol?
    private var view: AdvancedSearchViewProtocol?
    private var allBeersEntity: AllBeersEntity
    private var disposeBag = DisposeBag()
    private var isFiltering = false

    var beerId: Int?

    init(view: AdvancedSearchViewProtocol?, useCase: GetAllBeersByFoodUseCaseProtocol, router: AdvancedSearchRouterProtocol?, allBersEntity: AllBeersEntity) {
        self.useCase = useCase
        self.view = view
        self.allBeersEntity = allBersEntity
        self.router = router
    }
}

extension AdvancedSearchPresenter: AdvancedSearchPresenterProtocol {
    func navigateToBeerDetail(id: Int) {
        router?.pushBeerDetail(id: id)
    }

    func setIsFiltering(_ isFiltering: Bool) {
        self.isFiltering = false
    }

    func filterByABV(value: Int) {
        self.allBeersEntity.filterByABV(value: Double(value))
        self.isFiltering = true
        self.view?.loadData()
    }

    func cleanList() {
        self.allBeersEntity.allBeers = []
        self.allBeersEntity.filteredBeers = []
        self.view?.loadData()
    }

    func getBeerAt(atIndexPath indexPath: IndexPath) -> BeerEntity? {
        let list = isFiltering ? allBeersEntity.filteredBeers : allBeersEntity.allBeers

        if let list = list, !list.isEmpty, list.count > indexPath.row && indexPath.row >= 0 {
            return list[indexPath.row]
        }
        return nil
    }

    func getBeersByFood(food: String) {
        useCase?.getAllBeersByFood(food: food)?.subscribe(onNext: { list in
            self.view?.showLoading()
            self.allBeersEntity.allBeers += list
            self.view?.loadData()
        }, onError: { error in

        }, onCompleted: {

        }, onDisposed: {

        }).disposed(by: disposeBag)
    }

    func numberOfItems() -> Int? {
        return isFiltering ? allBeersEntity.filteredBeers?.count : allBeersEntity.allBeers.count
    }
}
