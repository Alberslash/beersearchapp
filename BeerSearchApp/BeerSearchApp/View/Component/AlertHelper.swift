//
//  AlertComponent.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 30/9/22.
//

import Foundation
import UIKit

class AlertHelper: UIAlertController {

    func createLoadingAlert(message: String) -> UIAlertController {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)

        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();

        //TODO: Check this out
        alert.view.addSubview(loadingIndicator)
        return alert
    }
}
