//
//  BeerTableViewCell.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 28/9/22.
//

import UIKit
import Kingfisher

class BeerTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var beerImageView: UIImageView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var firstBrewedLabel: UILabel!

    override func layoutSubviews() {
        super.layoutSubviews()
        self.containerView.frame = self.containerView.frame.inset(by: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
    }

    func configure(beer: BeerEntity) {

        if let url = beer.imageUrl {
            self.beerImageView.kf.setImage(with: URL(string: url), placeholder: #imageLiteral(resourceName: "img_beerPlaceholder"))
        }

        self.titleLabel.text = beer.name
        self.titleLabel.font = UIFont.boldSystemFont(ofSize: 16)
        self.titleLabel.textColor = UIColor.black
        self.titleLabel.numberOfLines = 2

        self.descriptionLabel.text = beer.description
        self.descriptionLabel.font = UIFont.systemFont(ofSize: 12)
        self.descriptionLabel.numberOfLines = 4
        self.descriptionLabel.textColor = UIColor.black

        self.firstBrewedLabel.text = beer.firstBrewed
        self.firstBrewedLabel.font = UIFont.systemFont(ofSize: 12)
        self.firstBrewedLabel.textColor = UIColor.black

        self.containerView.layer.cornerRadius = 8
        self.containerView.layer.borderWidth = 1
        self.containerView.layer.borderColor = UIColor.black.cgColor
    }
}
