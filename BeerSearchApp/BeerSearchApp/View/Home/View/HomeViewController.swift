//
//  HomeViewController.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 28/9/22.
//

import UIKit
import RxSwift
import RxCocoa

class HomeViewController: UIViewController {

    @IBOutlet var container: UIView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var advancedSearchLabel: UILabel!

    private var disposedBag = DisposeBag()

    private var isLoading: Bool = false
    private var alert: UIAlertController?

    var presenter: HomePresenterProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        setUpTableView()
        setUpObservers()
        presenter?.getAllBeers()
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter?.numberOfItems() ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: BeerTableViewCell.self)) as? BeerTableViewCell {
            if let beerEntity = presenter?.getBeerAt(atIndexPath: indexPath) {
                cell.configure(beer: beerEntity)
                cell.selectionStyle = .none
                return cell
            }
        }
        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let id = presenter?.getBeerAt(atIndexPath: indexPath)?.id  else {
            return
        }
        self.presenter?.navigateToBeerDetail(id: id)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pos = scrollView.contentOffset.y
        if let isPagingEnabled = presenter?.isPagingEnabled(), isPagingEnabled && pos > (tableView.contentSize.height - 50) - scrollView.frame.size.height {
            if !isLoading {
                isLoading = true
                loadMoreData()
            }
        }
    }
}

extension HomeViewController: HomeViewProtocol {
    func loadData() {
        self.tableView.reloadData()
    }

    func showLoading() {
        if let alert = alert {
            presenter?.presentAlert(alert: alert)
        }
    }

    func hideLoading() {
        presenter?.dismissAlert(alert: alert!)
        self.isLoading = false
    }
}

private extension HomeViewController {
    func setUpView() {
        self.view.backgroundColor = .white
        self.backView.backgroundColor = .clear
        self.container.backgroundColor = .white
        self.titleLabel.text = Localized.AllBeers
        self.titleLabel.numberOfLines = 2
        self.navigationItem.title = Localized.SearchPairing

        self.advancedSearchLabel.text = Localized.SearchPairing
        self.advancedSearchLabel.textColor = .blue

        self.advancedSearchLabel.font = UIFont.systemFont(ofSize: 14)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.advancedSearchLabel.addGestureRecognizer(tap)
        self.advancedSearchLabel.isUserInteractionEnabled = true

        self.alert = AlertHelper().createLoadingAlert(message: Localized.Loading)
    }

    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        presenter?.navigateToAdvancedSearch()
    }

    func setUpTableView() {
        self.tableView.backgroundColor = UIColor.white
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.showsVerticalScrollIndicator = false

        let beerCellCellIdentifier = String(describing: BeerTableViewCell.self)
        tableView.register(UINib(nibName: beerCellCellIdentifier, bundle: nil), forCellReuseIdentifier: beerCellCellIdentifier)
    }

    func setUpObservers() {
        self.searchBar.rx.text
            .distinctUntilChanged()
            .subscribe(onNext: { [unowned self] text in
                if let text = text, !text.isEmpty {
                    self.titleLabel.text = Localized.AllBeersWith + " \(text)"
                    presenter?.filterListByText(text: text)
                } else {
                    self.titleLabel.text = Localized.AllBeers
                    presenter?.setIsFiltering(false)
                }
                self.tableView.reloadData()
            }).disposed(by: disposedBag)
    }

    func loadMoreData() {
        presenter?.getAllBeers()
    }
}
