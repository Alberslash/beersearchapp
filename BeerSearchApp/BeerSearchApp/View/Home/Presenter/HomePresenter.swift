//
//  HomePresenter.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 28/9/22.
//

import Foundation
import RxSwift

class HomePresenter {
    private var useCase: GetAllBeersUseCaseProtocol
    private var view: HomeViewProtocol?
    private var disposeBag = DisposeBag()
    private var isFiltering = false

    private var allBeersEntity: AllBeersEntity
    private var router: HomeRouterProtocol?

    private var page: Int = 1

    init(view: HomeViewProtocol, useCase: GetAllBeersUseCaseProtocol, allBersEntity: AllBeersEntity, router: HomeRouterProtocol?) {
        self.useCase = useCase
        self.view = view
        self.allBeersEntity = allBersEntity
        self.router = router
    }
}
extension HomePresenter: HomePresenterProtocol {

    func dismissAlert(alert: UIAlertController){
        router?.dismissAlert(alert: alert)
    }

    func presentAlert(alert: UIAlertController) {
        router?.presentAlert(alert: alert)
    }

    func isPagingEnabled() -> Bool {
        return !isFiltering
    }

    func navigateToBeerDetail(id: Int) {
        router?.pushBeerDetail(id: id)
    }

    func navigateToAdvancedSearch() {
        router?.pushAdvancedSearchScene()
    }

    func setIsFiltering(_ isFiltering: Bool) {
        self.isFiltering = isFiltering
    }
    func filterListByText(text: String?) {
        if let text = text {
            allBeersEntity.filterByText(text: text)
            isFiltering = true
        } else {
            isFiltering = false
        }
        self.view?.loadData()
    }

    func getBeerAt(atIndexPath indexPath: IndexPath) -> BeerEntity? {
        let list = isFiltering ? allBeersEntity.filteredBeers : allBeersEntity.allBeers
        if let list = list, !list.isEmpty, list.count > indexPath.row && indexPath.row >= 0 {
            return list[indexPath.row]
        }
        return nil
    }

    func getAllBeers() {
        useCase.getAllBeers(page: self.page, offset: 25)?.subscribe(onNext: { list in
            self.view?.showLoading()
            self.allBeersEntity.allBeers += list
            self.page += 1
            self.view?.loadData()
        }, onError: { error in
            print(error)
        }, onCompleted: {
            self.view?.hideLoading()
        }).disposed(by: disposeBag)
    }

    func numberOfItems() -> Int? {
        return isFiltering ? allBeersEntity.filteredBeers?.count : allBeersEntity.allBeers.count
    }
}

private extension HomePresenter {

}
