//
//  HomeViewProtocol.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 30/9/22.
//

import Foundation

protocol HomeViewProtocol {
    func loadData()
    func hideLoading()
    func showLoading()
}
