//
//  HomePresenterProtocol.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 28/9/22.
//

import UIKit

protocol HomePresenterProtocol {

    func getAllBeers()
    func getBeerAt(atIndexPath indexPath: IndexPath) -> BeerEntity?
    func numberOfItems() -> Int?
    func filterListByText(text: String?)
    func navigateToAdvancedSearch()
    func setIsFiltering(_ isFiltering: Bool)
    func navigateToBeerDetail(id: Int)
    func isPagingEnabled() -> Bool
    func presentAlert(alert: UIAlertController)
    func dismissAlert(alert: UIAlertController)
}
