//
//  HomeConfigurator.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 28/9/22.
//

import UIKit

class HomeConfigurator {

    class func configureHomeScene() -> HomeViewController {
        let viewController = HomeViewController()
        let router = HomeRouter()
        let presenter = HomePresenter(
            view: viewController,
            useCase: GetAllBeersUseCase(
                service: BeerService()),
                allBersEntity: AllBeersEntity(allBeers: []),
                router: router
        )

        viewController.presenter = presenter
        router.viewController = viewController

        return viewController
    }
}

