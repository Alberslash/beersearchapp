//
//  HomeRouterProtocol.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 30/9/22.
//

import UIKit

protocol HomeRouterProtocol {

    func pushAdvancedSearchScene()

    func pushBeerDetail(id: Int)

    func presentAlert(alert: UIAlertController)

    func dismissAlert(alert: UIAlertController)
}
