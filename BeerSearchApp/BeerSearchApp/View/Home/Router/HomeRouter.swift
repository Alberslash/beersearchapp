//
//  HomeRouter.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 29/9/22.
//

import UIKit

class HomeRouter: HomeRouterProtocol {

    weak var viewController: UIViewController?

    func pushAdvancedSearchScene() {
        let advancedSearchDetailVC = AdvancedSearchConfigurator.configureAdvancedSearchScene()
        viewController?.navigationController?.pushViewController(advancedSearchDetailVC, animated: true)
    }

    func pushBeerDetail(id: Int) {
        let beerDetailVC = BeerDetailConfigurator.configureBeerDetailScene(id: id)
        viewController?.navigationController?.pushViewController(beerDetailVC, animated: true)
    }

    func presentAlert(alert: UIAlertController) {
        viewController?.navigationController?.present(alert, animated: true, completion: nil)
    }

    func dismissAlert(alert: UIAlertController) {
        alert.dismiss(animated: false)
    }
}
