//
//  BeerDetailPresenterProtocol.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 29/9/22.
//

import UIKit

protocol BeerDetailPresenterProtocol {

    func getBeerDetail()
    func numberOfItems() -> Int
    func getPairingAt(atIndexPath indexPath: IndexPath) -> String
}
