//
//  BeerDetailPresenter.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 29/9/22.
//

import Foundation
import RxSwift

class BeerDetailPresenter {

    private var beerId: Int
    private var view: BeerDetailViewProtocol?
    private var useCase: GetBeerDetailUseCaseProtocol?
    private let disposeBag = DisposeBag()
    private var pairingList: [String] = []

    var router: BeerDetailRouterProtocol?

    init(beerId: Int, view: BeerDetailViewProtocol, useCase: GetBeerDetailUseCaseProtocol?, router: BeerDetailRouterProtocol?) {
        self.view = view
        self.useCase = useCase
        self.beerId = beerId
        self.router = router
    }
}

extension BeerDetailPresenter: BeerDetailPresenterProtocol {

    func getPairingAt(atIndexPath indexPath: IndexPath) -> String {
        return self.pairingList[indexPath.row]
    }

    func numberOfItems() -> Int {
        pairingList.count
    }

    func getBeerDetail() {
        useCase?.getBeerDetail(id: beerId)?.subscribe(onNext: { beer in
            self.view?.showLoader()

                self.view?.updateViewWithBeerDetail(beer: beer)
                self.pairingList = beer.foodPairing ?? []
                self.view?.loadData()
            
        }, onError: { error in
            print(error)
        }, onCompleted: {
            self.view?.hideLoader()
        }).disposed(by: disposeBag)
    }
}

private extension BeerDetailPresenter {


}
