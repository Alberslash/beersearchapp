//
//  BeerDetailView.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 29/9/22.
//

import Foundation

protocol BeerDetailViewProtocol {
    func updateViewWithBeerDetail(beer: BeerEntity)
    func loadData()
    func showLoader()
    func hideLoader()
}
