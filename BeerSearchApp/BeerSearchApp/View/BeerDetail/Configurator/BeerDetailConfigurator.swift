//
//  BeerDetailConfigurator.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 29/9/22.
//

import UIKit

class BeerDetailConfigurator {

    class func configureBeerDetailScene(id: Int) -> BeerDetailController {
        let viewController = BeerDetailController()
        let router = BeerDetailRouter()
        let presenter = BeerDetailPresenter(
            beerId: id,
            view: viewController,
            useCase: GetBeerDetailUseCase(service: BeerService()),
            router: router
        )
        viewController.presenter = presenter
        router.viewController = viewController
        return viewController
    }
}
