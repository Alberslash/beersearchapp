//
//  BeerDetailController.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 29/9/22.
//

import UIKit
import RxSwift

class BeerDetailController: UIViewController {

    @IBOutlet var contentView: UIView!

    @IBOutlet weak var beerImageView: UIImageView!
    @IBOutlet weak var beerTitleLabel: UILabel!
    @IBOutlet weak var beerDescriptionLabel: UILabel!

    @IBOutlet weak var pairingImageView: UIImageView!
    @IBOutlet weak var pairingTitleLabel: UILabel!
    @IBOutlet weak var pairingCollectionView: UICollectionView!

    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loader: UIActivityIndicatorView!

    private var disposedBag = DisposeBag()
    private var alert: UIAlertController?

    var presenter: BeerDetailPresenterProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpCollectionView()
        setupView()
        self.presenter?.getBeerDetail()
    }
}

extension BeerDetailController: BeerDetailViewProtocol {
    func showLoader() {
        self.loaderView.isHidden = false
        self.loader.startAnimating()
    }

    func hideLoader() {
        self.loaderView.isHidden = true
        self.loader.stopAnimating()
    }

    func loadData() {
        self.pairingCollectionView.reloadData()
    }

    func updateViewWithBeerDetail(beer: BeerEntity) {
        if let image = beer.imageUrl {
            self.beerImageView.kf.setImage(with: URL(string: image), placeholder: #imageLiteral(resourceName: "img_beerPlaceholder"))
        }
        self.beerTitleLabel.text = beer.name

        self.beerDescriptionLabel.text = beer.description

        self.pairingImageView.image = UIImage(named: "maridaje")
        self.pairingImageView.contentMode = .scaleAspectFit

        self.pairingTitleLabel.text = Localized.Pairings
    }
}

extension BeerDetailController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.presenter?.numberOfItems() ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: PairingCollectionViewCell.self), for: indexPath) as? PairingCollectionViewCell {
            if let pairing = presenter?.getPairingAt(atIndexPath: indexPath) {
                cell.configure(pairingTitle: pairing)
            }
            return cell
        }
        return UICollectionViewCell()
    }

    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        let bounds = UIScreen.main.bounds
        return CGSize(width: CGFloat(bounds.width - 120), height: 120)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
    }
}

private extension BeerDetailController {

    func setupView() {
        self.navigationItem.title = Localized.BeerDetail

        self.beerTitleLabel.font = UIFont.boldSystemFont(ofSize: 18)
        self.beerTitleLabel.textColor = .red

        self.beerDescriptionLabel.numberOfLines = 0

        self.alert = AlertHelper().createLoadingAlert(message: Localized.Loading)

        self.loaderView.isHidden = false

        self.loader.tintColor = .red
        self.loader.style = .large
    }

    func setUpCollectionView() {
        let cellIdentifier = String(describing: PairingCollectionViewCell.self)
        pairingCollectionView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)

        self.pairingCollectionView.delegate = self
        self.pairingCollectionView.dataSource = self

        self.pairingCollectionView.showsHorizontalScrollIndicator = false

        self.beerDescriptionLabel.textColor = .gray
        self.beerDescriptionLabel.font = UIFont.systemFont(ofSize: 14)

    }
}
