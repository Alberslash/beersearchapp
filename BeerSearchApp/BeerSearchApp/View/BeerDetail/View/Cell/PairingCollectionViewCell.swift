//
//  PairingCollectionViewCell.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 30/9/22.
//

import Foundation
import UIKit

class PairingCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var pairingTitle: UILabel!
    @IBOutlet weak var backview: UIView!

    func configure(pairingTitle: String) {
        self.pairingTitle.text = pairingTitle
        self.pairingTitle.numberOfLines = 0

        self.backview.layer.borderWidth = 1
        self.backview.layer.borderColor = UIColor.red.cgColor
        self.layer.cornerRadius = 4
    }

}
