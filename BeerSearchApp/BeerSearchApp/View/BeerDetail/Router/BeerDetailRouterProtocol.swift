//
//  BeerDetailRouterProtocol.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 30/9/22.
//

import UIKit

protocol BeerDetailRouterProtocol {
    
    func presentAlert(alert: UIAlertController)

    func dismissAlert(alert: UIAlertController)
}
