//
//  BeerDetailRouter.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 30/9/22.
//

import UIKit

class BeerDetailRouter: BeerDetailRouterProtocol {

    weak var viewController: UIViewController?

    func presentAlert(alert: UIAlertController) {
        viewController?.navigationController?.present(alert, animated: true, completion: nil)
    }

    func dismissAlert(alert: UIAlertController) {
        alert.dismiss(animated: false)
    }
}
