//
//  StringExtension.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 2/10/22.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: self)
    }
}
