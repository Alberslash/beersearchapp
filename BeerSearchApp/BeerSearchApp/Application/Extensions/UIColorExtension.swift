//
//  UIColorExtension.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 28/9/22.
//

import Foundation
import UIKit

extension UIColor {
    static let primaryColor = #colorLiteral(red: 1, green: 0, blue: 0.1825963855, alpha: 1)
    static let secondaryColor = #colorLiteral(red: 1, green: 0, blue: 0.1825963855, alpha: 0.6919921045)
}
