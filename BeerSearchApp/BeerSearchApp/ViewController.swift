//
//  ViewController.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 28/9/22.
//

import UIKit
import Lottie

class ViewController: UIViewController {

    private var animationView: AnimationView?

    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()

        animationView = .init(name: "beerDancing")

        animationView!.frame = view.bounds
        animationView!.contentMode = .scaleAspectFit
        animationView!.loopMode = .loop
        animationView!.animationSpeed = 0.5

        view.addSubview(animationView!)

        animationView!.play()

        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.presentHomeViewController()
        }

    }

    func presentHomeViewController() {
        let homeViewController = HomeConfigurator.configureHomeScene()
        let navigationController = UINavigationController(rootViewController: homeViewController)
        navigationController.modalPresentationStyle = .fullScreen

        UIApplication.shared.keyWindow?.rootViewController = navigationController
    }

    func configureUI() {
        self.view.backgroundColor = UIColor.white
    }

}

