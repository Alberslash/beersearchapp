//
//  BeerDTO.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 28/9/22.
//

import Foundation

struct BeerDTO: Decodable {
    var id: Int?
    var name: String?
    var tagline: String?
    var firstBrewed: String?
    var description: String?
    var imageUrl: String?
    var alcoholByVolume: Double?
    var internationalBitteringUnit: Double?
    var targetFG: Double?
    var targetOG: Double?
    var europeanBreweryConvention: Double?
    var scaleMeasuringColorIntensity: Double?
    var ph: Double?
    var attenuationLevel: Double?
    var volume: VolumeDTO?
    var boilVolume: BoilVolumeDTO?
    //var method: MethodDTO?
    //var ingredients: IngredientsDTO?
    var foodPairing: [String]?
    var brewersTips: String?
    var contributedBy: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case tagline = "tagline"
        case firstBrewed = "first_brewed"
        case description = "description"
        case imageUrl = "image_url"
        case alcoholByVolume = "abv"
        case internationalBitteringUnit = "ibu"
        case targetFG = "target_fg"
        case targetOG = "target_og"
        case europeanBreweryConvention = "ebc"
        case scaleMeasuringColorIntensity = "srm"
        case ph = "ph"
        case attenuationLevel = "attenuation_level"
        case volume = "volume"
        case boilVolume = "boil_volume"
       // case method = "method"
       // case ingredients = "ingredients"
        case brewersTips = "brewers_tips"
        case foodPairing = "food_pairing"
        case contributedBy = "contributed_by"
    }

    init(decode decoder: Decoder) throws {
        let decoder = try decoder.container(keyedBy: CodingKeys.self)
        id = try decoder.decodeIfPresent(Int.self, forKey: .id)
        name = try decoder.decodeIfPresent(String.self, forKey: .name)
        tagline = try decoder.decodeIfPresent(String.self, forKey: .tagline)
        firstBrewed = try decoder.decodeIfPresent(String.self, forKey: .firstBrewed)
        description = try decoder.decodeIfPresent(String.self, forKey: .description)
        imageUrl = try decoder.decodeIfPresent(String.self, forKey: .imageUrl)
        alcoholByVolume = try decoder.decodeIfPresent(Double.self, forKey: .alcoholByVolume)
        internationalBitteringUnit = try decoder.decodeIfPresent(Double.self, forKey: .internationalBitteringUnit)
        targetFG = try decoder.decodeIfPresent(Double.self, forKey: .targetFG)
        targetOG = try decoder.decodeIfPresent(Double.self, forKey: .targetOG)
        europeanBreweryConvention = try decoder.decodeIfPresent(Double.self, forKey: .europeanBreweryConvention)
        scaleMeasuringColorIntensity = try decoder.decodeIfPresent(Double.self, forKey: .scaleMeasuringColorIntensity)
        ph = try decoder.decodeIfPresent(Double.self, forKey: .ph)
        attenuationLevel = try decoder.decodeIfPresent(Double.self, forKey: .attenuationLevel)
        volume = try decoder.decodeIfPresent(VolumeDTO.self, forKey: .volume)
        boilVolume = try decoder.decodeIfPresent(BoilVolumeDTO.self, forKey: .boilVolume)
        //method = try decoder.decodeIfPresent(MethodDTO.self, forKey: .method)
       // ingredients = try decoder.decodeIfPresent(IngredientsDTO.self, forKey: .ingredients)
        foodPairing = try decoder.decodeIfPresent([String].self, forKey: .foodPairing)
        brewersTips = try decoder.decodeIfPresent(String.self, forKey: .brewersTips)
        contributedBy = try decoder.decodeIfPresent(String.self, forKey: .contributedBy)
    }
}
