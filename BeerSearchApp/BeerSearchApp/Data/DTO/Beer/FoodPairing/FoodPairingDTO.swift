//
//  FoodPairingDTO.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 28/9/22.
//

import Foundation

struct FoodPairingDTO: Decodable {
    var food_pairing: [String]?
}
