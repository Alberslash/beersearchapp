//
//  BeerService.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 28/9/22.
//

import Foundation
import RxSwift

class BeerService: BeerRepository {

    func getAllBeers(parameters: [String : Int], isBody: Bool) -> Observable<Response<[BeerDTO]>> {
        let service = AlamofireWrapper<[BeerDTO]>.init(url: "https://api.punkapi.com/v2/beers", method: .get, parameters: parameters, isBody: isBody)
        return service.execute()
    }

    func getBeer(id: Int) -> Observable<Response<[BeerDTO]>> {
        let service = AlamofireWrapper<[BeerDTO]>.init(url: "https://api.punkapi.com/v2/beers/\(id)", method: .get, isBody: true)
        return service.execute()
    }

    func getAllBeersByFood(parameters: [String : String], isBody: Bool) -> Observable<Response<[BeerDTO]>> {
        let service = AlamofireWrapper<[BeerDTO]>.init(url: "https://api.punkapi.com/v2/beers", method: .get, parameters: parameters, isBody: isBody)
        return service.execute()
    }
}
