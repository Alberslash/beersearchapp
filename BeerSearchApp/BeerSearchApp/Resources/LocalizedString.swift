//
//  LocalizedString.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 2/10/22.
//

import Foundation

enum Localized {
    static let AllBeers = "AllBeers".localized
    static let SearchBeers = "SearchBeers".localized
    static let SearchPairing = "SearchPairing".localized
    static let Loading = "Loading".localized
    static let AllBeersWith = "AllBeersWith".localized
    static let AbvPercentage = "AbvPercentage".localized
    static let SearchBeersToEat = "SearchBeersToEat".localized
    static let SearchingBeersToEat = "SearchingBeersToEat".localized
    static let Pairings = "Pairings".localized
    static let BeerDetail = "BeerDetail".localized
}
