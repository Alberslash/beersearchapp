//
//  BeerReposityor.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 28/9/22.
//

import Foundation
import RxSwift

protocol BeerRepository {

    func getAllBeers(parameters: [String : Int], isBody: Bool) -> Observable<Response<[BeerDTO]>>

    func getBeer(id: Int) -> Observable<Response<[BeerDTO]>>

    func getAllBeersByFood(parameters: [String : String], isBody: Bool) -> Observable<Response<[BeerDTO]>>
}
