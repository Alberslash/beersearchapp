//
//  BeersMapper.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 29/9/22.
//

import Foundation

struct BeerMappers {

    func mappBeers(listOfBeers: [BeerDTO]) -> [BeerEntity]{
        var beerList = [BeerEntity]()
        for beer in listOfBeers {
            beerList.append(mapBeer(beer: beer))
        }
        return beerList
    }

    func mapBeer(beer: BeerDTO) -> BeerEntity {

        let volume = VolumeEntity(value: beer.volume?.value, unit: beer.volume?.unit)
        let boilVolume =  BoilVolumeEntity(value: beer.boilVolume?.value, unit: beer.boilVolume?.unit)
        var listOfPairings: [String] = []
        if let pairingList = beer.foodPairing {
            for pairing in pairingList {
                listOfPairings.append(pairing)
            }
        }

        return BeerEntity(
            id: beer.id,
            name: beer.name,
            tagline: beer.tagline,
            firstBrewed: beer.firstBrewed,
            description: beer.description,
            imageUrl: beer.imageUrl,
            alcoholByVolume: beer.alcoholByVolume,
            internationalBitteringUnit: beer.internationalBitteringUnit,
            targetFG: beer.targetFG,
            targetOG: beer.targetOG,
            europeanBreweryConvention: beer.europeanBreweryConvention,
            scaleMeasuringColorIntensity: beer.scaleMeasuringColorIntensity,
            ph: beer.ph,
            attenuationLevel: beer.attenuationLevel,
            volume: volume,
            boilVolume: boilVolume,
            foodPairing: listOfPairings,
            brewersTips: beer.brewersTips,
            contributedBy: beer.contributedBy
        )
    }
}
