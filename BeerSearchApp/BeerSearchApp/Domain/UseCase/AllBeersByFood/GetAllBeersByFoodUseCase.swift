//
//  GetAllBeersByFoodUseCase.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 29/9/22.
//

import Foundation
import RxSwift

class GetAllBeersByFoodUseCase {

    private var service: BeerRepository?

    init(service: BeerRepository?){
        self.service = service
    }
}

extension GetAllBeersByFoodUseCase: GetAllBeersByFoodUseCaseProtocol {
    
    func getAllBeersByFood(food: String) -> Observable<[BeerEntity]>? {
        let parameters: [String : String] = [
            "food" : food
        ]
        return service?.getAllBeersByFood(parameters: parameters, isBody: false).map { data -> [BeerEntity] in
            if let listOfBeers = data.body {
                return BeerMappers().mappBeers(listOfBeers: listOfBeers)
            }
            return []
        }.asObservable()
    }
}
