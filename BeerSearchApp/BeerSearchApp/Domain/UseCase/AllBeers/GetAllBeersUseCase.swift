//
//  GetAllBeersUseCase.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 28/9/22.
//

import Foundation
import RxSwift

class GetAllBeersUseCase: GetAllBeersUseCaseProtocol {

    var service: BeerRepository?

    init(service: BeerRepository?) {
        self.service = service
    }

    func getAllBeers(page: Int, offset: Int) -> Observable<[BeerEntity]>? {
        let parameters: [String : Int] = [
            "page" : page,
            "per_page" : offset
        ]
        return service?.getAllBeers(parameters: parameters, isBody: false).map { data -> [BeerEntity] in
            if let listOfBeers = data.body {
                return BeerMappers().mappBeers(listOfBeers: listOfBeers)
            }
            return []
        }.asObservable()
    }
}
