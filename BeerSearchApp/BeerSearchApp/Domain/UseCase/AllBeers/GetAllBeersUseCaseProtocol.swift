//
//  GetAllBeersUseCaseProtocol.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 28/9/22.
//

import Foundation
import RxSwift

protocol GetAllBeersUseCaseProtocol {
    func getAllBeers(page: Int, offset: Int) -> Observable<[BeerEntity]>?
}
