//
//  GetBeerDetailUseCase.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 29/9/22.
//

import Foundation
import RxSwift

class GetBeerDetailUseCase {

    private var service: BeerRepository?

    init(service: BeerRepository?) {
        self.service = service
    }
}

extension GetBeerDetailUseCase: GetBeerDetailUseCaseProtocol {

    func getBeerDetail(id: Int) -> Observable<BeerEntity>? {
        return service?.getBeer(id: id).map { response -> BeerEntity in
            if let beer = response.body, !beer.isEmpty {
                return BeerMappers().mapBeer(beer: beer[0])
            }
            return BeerEntity(id: -1)
        }
    }
}
