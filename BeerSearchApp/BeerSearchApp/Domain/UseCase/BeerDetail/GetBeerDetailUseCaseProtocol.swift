//
//  GetBeerDetailUseCaseProtocol.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 29/9/22.
//

import Foundation
import RxSwift

protocol GetBeerDetailUseCaseProtocol {
    func getBeerDetail(id: Int) -> Observable<BeerEntity>?
}

