//
//  AllBeersEntity.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 28/9/22.
//

import Foundation

struct AllBeersEntity {
    var allBeers: [BeerEntity]
    var filteredBeers: [BeerEntity]?

    mutating func filterByText(text: String) {
        let text = text.uppercased()
        filteredBeers = allBeers.filter { beer in
            if let beerName = beer.name {
                return beerName.lowercased().contains(text.lowercased())
            }
            return false
        }
    }

    mutating func filterByABV(value: Double) {
        filteredBeers = allBeers.filter { beer in
            if let abv = beer.alcoholByVolume {
                return abv >= value
            }
            return false
        }
    }
}
