//
//  FoodPairingEntity.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 28/9/22.
//

import Foundation

struct FoodPairingEntity {
    var foodPairing: [String]?
}
