//
//  BoilVolumeEntity.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 28/9/22.
//

import Foundation

struct BoilVolumeEntity {
    var value: Int?
    var unit: String?
}
