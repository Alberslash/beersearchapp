//
//  BeerEntity.swift
//  BeerSearchApp
//
//  Created by Alberto Fernandez-Baillo Rodriguez on 28/9/22.
//

import Foundation

struct BeerEntity {
    var id: Int?
    var name: String?
    var tagline: String?
    var firstBrewed: String?
    var description: String?
    var imageUrl: String?
    var alcoholByVolume: Double?
    var internationalBitteringUnit: Double?
    var targetFG: Double?
    var targetOG: Double?
    var europeanBreweryConvention: Double?
    var scaleMeasuringColorIntensity: Double?
    var ph: Double?
    var attenuationLevel: Double?
    var volume: VolumeEntity?
    var boilVolume: BoilVolumeEntity?
    var foodPairing: [String]?
    var brewersTips: String?
    var contributedBy: String?
}
