# Buscador de Cervezas

Esta aplicación desallorada con swift sobre UIKit consiste en un buscador de cervezas obteniendo los datos de una API.

La pantalla inicial es un buscador sobre todas las cervezas que devuelve la api, se muestra una tabla con paginación mostratond titulo, unas líneas de descripción y el año de fabricación.

Si hacemos click sobre busqueda de maridajes accedemos a otro buscador con un slider, en este buscador se necesitara mínimo haber escrito 4 letras en el buscador para que se muestre información sobre cuales son las mejores cervezas según la comida que hayamos escrito, además se permite al usuario elegir con un slider el nivel de porcetaje de alcohol que desea que tenga la cerveza.

Por último se ha implementado una pantalla con el detalle de cada cerveza.

La aplicación se puede ejecutar tanto en disposito movil como en ipad (Para ipad sólo se han modificado algunos constraints para que se vea algo mejor)

## Instalación

El proyecto esta preparado para ejecutarse con CocoaPods, para ello se deberá de acceder en el terminal a la ruta en la que se haya descargado el proyecto y en /BeerSearchApp/BeerSearchApp se deberá de ejecutar el comando pod install y ejecutando el archivo acabo en .xcworkspace ya se abrira xCode para poder ejecutar el proyecto.

## Requerimientos

La versión mínima establecida es iOS 13.0

## Arquitectura

Para el desarrollo de esta aplicación se ha optado por una Arquitectura MVP con capas adicionales que se explican a continuación:

 - View: Capa encargada única y exclusivamente de mostrar la interfaz de usuario y de actualizarse
 - Presenter: Capa encargada de manejar la lógica de la vista
 - UseCase: Capa encargada de controlar la lógico de obtencion de datos para saber si ha de cogerla de una API o de una caché o de otra BBDD, favorece al   d  desacople del código en la aplicación y ayuda a evitar tener código duplicado y centralizado en un único punto, además se encarga de volcar los datos recibidos por la capa de datos en forma de DTOs en un objeto de negocio de la capa de dominio que se usara en toda la aplicación.
 - Configurator: Capa encargada de generar todas las instancias para el flujo necesario de una feature.
 - Router: Capa encargada mostrar, ocultar presenter, componentes y vistas.

 ![diagrama de clases](diagrama_de_clases.png)
 
